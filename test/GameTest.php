<?php

declare(strict_types=1);

namespace BoardGame\Test;

use BoardGame\DateTimeFactory;
use BoardGame\Game;
use BoardGame\NativeDateTimeFactory;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    const WINNING_X = 3;
    const WINNING_Y = 2;

    private $game;

    public function setUp()
    {
        $this->game = new Game(new NativeDateTimeFactory(), self::WINNING_X, self::WINNING_Y);
    }

    /**
     * @expectedException \BoardGame\Exception\OutOfBoundsException
     */
    public function testCreateXOutOfLowerBound()
    {
        new Game(new NativeDateTimeFactory(), 0, 1);
    }

    /**
     * @expectedException \BoardGame\Exception\OutOfBoundsException
     */
    public function testCreateXOutOfUpperBound()
    {
        new Game(new NativeDateTimeFactory(), Game::MAX_X + 1, 1);
    }

    /**
     * @expectedException \BoardGame\Exception\OutOfBoundsException
     */
    public function testCreateYOutOfLowerBound()
    {
        new Game(new NativeDateTimeFactory(), 1, 0);
    }

    /**
     * @expectedException \BoardGame\Exception\OutOfBoundsException
     */
    public function testCreateYOutOfUpperBound()
    {
        new Game(new NativeDateTimeFactory(), 1, Game::MAX_Y + 1);
    }

    public function testAllFailures()
    {
        $this->assertFalse($this->game->guess(1, 1));
        $this->assertFalse($this->game->guess(1, 2));
        $this->assertFalse($this->game->guess(2, 1));
        $this->assertFalse($this->game->guess(2, 2));
        $this->assertFalse($this->game->guess(2, 3));
    }

    /**
     * @expectedException \BoardGame\Exception\OutOfBoundsException
     */
    public function testGuessXOutOfLowerBound()
    {
        $this->game->guess(0, 1);
    }

    /**
     * @expectedException \BoardGame\Exception\OutOfBoundsException
     */
    public function testGuessYOutOfLowerBound()
    {
        $this->game->guess(1, 0);
    }

    /**
     * @expectedException \BoardGame\Exception\OutOfBoundsException
     */
    public function testGuessXOutOfUpperBound()
    {
        $this->game->guess(Game::MAX_X + 1, 1);
    }

    /**
     * @expectedException \BoardGame\Exception\OutOfBoundsException
     */
    public function testGuessYOutOfUpperBound()
    {
        $this->game->guess(1, Game::MAX_X + 1);
    }

    public function testSuccess()
    {
        $this->assertTrue($this->win());
    }

    /**
     * @expectedException \BoardGame\Exception\TooManyGuessesException
     */
    public function testTooManyGuesses()
    {
        $this->game->guess(1, 1);
        $this->game->guess(1, 2);
        $this->game->guess(2, 1);
        $this->game->guess(2, 2);
        $this->game->guess(2, 3);
        $this->game->guess(2, 4);
    }

    /**
     * @expectedException \BoardGame\Exception\TimeoutExpiredException
     */
    public function testTimeOut()
    {
        $fakeDateTimeFactory = new class implements DateTimeFactory {
            private $counter = 0;

            public function getDateTime(): DateTimeImmutable
            {
                $timeout = Game::TIMEOUT;

                if (0 === $this->counter) {
                    $time = new DateTimeImmutable('@0');
                } else {
                    $time = new DateTimeImmutable("@$timeout");
                }

                $this->counter++;

                return $time;
            }
        };

        $game = new Game($fakeDateTimeFactory, self::WINNING_X, self::WINNING_Y);

        $game->guess(1, 1);
        $game->guess(self::WINNING_X, self::WINNING_Y);
    }

    /**
     * @expectedException \BoardGame\Exception\RepeatedGuessException
     */
    public function testRepeatedGuess()
    {
        $this->game->guess(1, 1);
        $this->game->guess(1, 1);
    }

    /**
     * @expectedException \BoardGame\Exception\GameFinishedException
     */
    public function testGuessAfterGameIsFinished()
    {
        $this->win();
        $this->game->guess(1, 1);
    }

    private function win(): bool
    {
        return $this->game->guess(self::WINNING_X, self::WINNING_Y);
    }
}
