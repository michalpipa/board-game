<?php

declare(strict_types=1);

namespace BoardGame\Exception;

use \Exception;

class OutOfBoundsException extends Exception
{
}
