<?php

declare(strict_types=1);

namespace BoardGame;

use DateTimeImmutable;

class NativeDateTimeFactory implements DateTimeFactory
{
    public function getDateTime(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }
}
