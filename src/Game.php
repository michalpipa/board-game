<?php

declare(strict_types=1);

namespace BoardGame;

use BoardGame\Exception\GameFinishedException;
use BoardGame\Exception\OutOfBoundsException;
use BoardGame\Exception\RepeatedGuessException;
use BoardGame\Exception\TimeoutExpiredException;
use BoardGame\Exception\TooManyGuessesException;

class Game
{
    const MAX_X = 5;
    const MAX_Y = 4;
    const GUESS_LIMIT = 5;
    const TIMEOUT = 60;

    private $winningToken;
    private $guesses = [];
    private $firstGuessTime;
    private $dateTimeFactory;

    public function __construct(DateTimeFactory $dateTimeFactory, int $x, int $y)
    {
        $this->checkBounds($x, $y);

        $this->dateTimeFactory = $dateTimeFactory;

        $this->winningToken = [$x, $y];
    }

    public function guess(int $x, int $y): bool
    {
        $guess = [$x, $y];

        $this->checkBounds($x, $y);

        if ($this->isFinished()) {
            throw new GameFinishedException();
        }

        if ($this->isGuessLimitReached()) {
            throw new TooManyGuessesException();
        }

        if ($this->isRepeatedGuess($guess)) {
            throw new RepeatedGuessException();
        }

        $this->registerGuess($guess);

        if ($this->isWinningGuess($guess)) {
            return true;
        }

        return false;
    }

    private function registerGuess(array $guess)
    {
        if ($this->isFirstGuess()) {
            $this->registerFirstGuess();
        } else {
            if ($this->timeoutExpired()) {
                throw new TimeoutExpiredException();
            }
        }

        $this->guesses[] = $guess;
    }

    private function timeoutExpired(): bool
    {
        $now = $this->dateTimeFactory->getDateTime();
        $interval = self::TIMEOUT - 1;

        return $this->firstGuessTime->modify("+$interval seconds") < $now;
    }

    private function isWinningGuess(array $guess): bool
    {
        if ($guess === $this->winningToken) {
            return true;
        }

        return false;
    }

    private function checkBounds(int $x, int $y)
    {
        if ($this->isXOutOfBounds($x) or $this->isYOutOfBounds($y)) {
            throw new OutOfBoundsException();
        }
    }

    private function isXOutOfBounds(int $x): bool
    {
        return $x < 1 or $x > self::MAX_X;
    }

    private function isYOutOfBounds(int $y): bool
    {
        return $y < 1 or $y > self::MAX_Y;
    }

    private function isRepeatedGuess($guess): bool
    {
        return in_array($guess, $this->guesses);
    }

    private function isFinished(): bool
    {
        return in_array($this->winningToken, $this->guesses);
    }

    private function isGuessLimitReached(): bool
    {
        return self::GUESS_LIMIT === count($this->guesses);
    }

    private function isFirstGuess(): bool
    {
        return null === $this->firstGuessTime;
    }

    private function registerFirstGuess()
    {
        $this->firstGuessTime = $this->dateTimeFactory->getDateTime();
    }
}
