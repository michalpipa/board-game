<?php

declare(strict_types=1);

namespace BoardGame\Exception;

use Exception;

class GameFinishedException extends Exception
{
}
