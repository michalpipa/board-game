<?php

declare(strict_types=1);

namespace BoardGame;

use DateTimeImmutable;

interface DateTimeFactory
{
    public function getDateTime(): DateTimeImmutable;
}
